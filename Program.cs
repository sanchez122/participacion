﻿using System;

    namespace Colaboracion1
           
        {
                             ///PARTICIPACION///
        class Cliente
        {
            private string NOM;s
            private int montos;

            public Cliente(string nom)
            {
                NOM = nom;
                montos = 0;
            }

            public void Deposita(int m)
            {
                montos = montos + m;
            }

            public void Extrae(int m)
            {
                montos = montos - m;
            }
            
              
              //RetornaMontos//
            public int RetornaMonto()
            {


                return montos;
            }

            public void Fijar()
            {

                Console.WriteLine(NOM + " su ahorro de deposito es de " + montos);


            }
        }

        class Banco
        {
            private Cliente cli0, cli1, cli2;

            public Banco()
            {
                cli0 = new Cliente("ANDREINA");
                cli1 = new Cliente("ANDREA");
                cli2 = new Cliente("ANDRES");
            }

            public void Operar()
            {
                cli0.Deposita(500);
                cli1.Deposita(700);
                cli2.Deposita(900);
                
            }

            public void DT()
            {
                int t = cli0.RetornaMonto() +
                        cli1.RetornaMonto() +
                        cli2.RetornaMonto();
             

                Console.WriteLine("La cantidad en total de su dinero es de:" + t);


                cli0.Fijar();
                cli1.Fijar();
                cli2.Fijar();
            }




            static void Main(string[] args)
            {
                Banco banco0 = new Banco();
                banco0.Operar();
                banco0.DT();

                Console.ReadKey();
            }
        }
    }

    

